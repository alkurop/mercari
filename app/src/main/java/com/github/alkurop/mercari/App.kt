package com.github.alkurop.mercari

import android.app.Application
import com.github.alkurop.mercari.api.DependencyModule
import com.github.alkurop.mercari.di.AppComponent
import com.github.alkurop.mercari.di.DaggerAppComponent

class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
            .builder()
            .dependencyModule(DependencyModule(this))
            .build()
    }
}