package com.github.alkurop.mercari

import java.util.concurrent.atomic.AtomicBoolean

class OneShot<T>(private val value: T) {
    private val isShot = AtomicBoolean()

    fun getValueOnce(): T {
        isShot.set(true)
        return value
    }

    init {
        isShot.set(false)
    }

    fun checkValue(): T {
        return value
    }

    fun isShot(): Boolean {
        return isShot.get()
    }
}
