package com.github.alkurop.mercari.api

import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import io.reactivex.Single


interface Api {

    fun getData(): Single<Array<Data>>

    fun getItems(jsonUrl: String): Single<Array<Item>>

}