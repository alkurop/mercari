package com.github.alkurop.mercari.api

import dagger.Binds
import dagger.Module

@Module
interface ApiModule {

    @Binds
    fun provideApi(api: OkHttpApi): Api

    @Binds
    fun provideNetworkChecker(networkChecker: ContextNetworkChecker): NetworkChecker
}