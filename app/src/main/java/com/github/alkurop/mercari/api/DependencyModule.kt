package com.github.alkurop.mercari.api

import android.content.Context
import com.github.alkurop.mercari.config.ApiConfig
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DependencyModule(private val context: Context) {

    @Singleton
    @Provides
    fun provideOkHttp(config: ApiConfig): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(config.requestTimeoutSeconds, TimeUnit.SECONDS)
            .readTimeout(config.requestTimeoutSeconds, TimeUnit.SECONDS)
            .writeTimeout(config.requestTimeoutSeconds, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideConfig(): ApiConfig {
        return ApiConfig(
            "https://s3-ap-northeast-1.amazonaws.com/m-et/Android/json/master.json",
            10
        )
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    fun provideScheduler(): Scheduler {
        return Schedulers.io()
    }

    @Provides
    fun provideContext(): Context {
        return context
    }
}