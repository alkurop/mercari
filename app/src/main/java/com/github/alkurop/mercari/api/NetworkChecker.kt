package com.github.alkurop.mercari.api

interface NetworkChecker {

    fun isConnected(): Boolean

}