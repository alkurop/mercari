package com.github.alkurop.mercari.api

import android.accounts.NetworkErrorException
import com.github.alkurop.mercari.config.ApiConfig
import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import javax.inject.Inject


class OkHttpApi @Inject constructor(
    private val okHttpClient: OkHttpClient,
    private val apiConfig: ApiConfig,
    private val gson: Gson,
    private val networkChecker: NetworkChecker
) : Api {

    override fun getData(): Single<Array<Data>> {
        val request = Single
            .fromCallable {
                val request = Request.Builder()
                    .url(apiConfig.baseUrl)
                    .build()
                val response = okHttpClient.newCall(request).execute()
                if (!response.isSuccessful) throw  IOException("Unexpected code " + response);
                val string = response.body()?.string() ?: throw  IOException("Empty body")
                response.body()?.close()
                gson.fromJson(string, Array<Data>::class.java)
            }
        return checkNetwork<Array<Data>>().ambWith(request)
    }

    override fun getItems(jsonUrl: String): Single<Array<Item>> {

        val request = Single
            .fromCallable {
                val request = Request.Builder()
                    .url(jsonUrl)
                    .build()
                val response = okHttpClient.newCall(request).execute()
                if (!response.isSuccessful) throw  IOException("Unexpected code " + response);
                val string = response.body()?.string() ?: throw  IOException("Empty body")
                response.body()?.close()
                gson.fromJson(string, Array<Item>::class.java)
            }
        return checkNetwork<Array<Item>>().ambWith(request)
    }

    private fun <T> checkNetwork(): Single<T> {
        return if (networkChecker.isConnected()) Single.never<T>()
        else Single.error(NetworkErrorException("Not connected"))
    }
}