package com.github.alkurop.mercari.config

data class ApiConfig(
    val baseUrl: String,
    val requestTimeoutSeconds: Long
)