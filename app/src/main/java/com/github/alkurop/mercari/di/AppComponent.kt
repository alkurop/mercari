package com.github.alkurop.mercari.di

import com.github.alkurop.mercari.api.ApiModule
import com.github.alkurop.mercari.api.DependencyModule
import com.github.alkurop.mercari.home.MainActivity
import com.github.alkurop.mercari.list.ItemListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApiModule::class,
        DependencyModule::class
    ]
)
interface AppComponent {

    fun inject(activity: MainActivity)

    fun inject(activity: ItemListFragment)
}