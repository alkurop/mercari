package com.github.alkurop.mercari.domain

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("name") val name: String,
    @SerializedName("data") val dataUrl: String
)