package com.github.alkurop.mercari.domain

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("status") val status: Status,
    @SerializedName("num_likes") val likeCount: Int,
    @SerializedName("num_comments") val commentCount: Int,
    @SerializedName("price") val price: Int,
    @SerializedName("photo") val photoUrl: String
)

enum class Status{
    @SerializedName("on_sale") ON_SALE,
    @SerializedName("sold_out") SOLD_OUT
}