package com.github.alkurop.mercari.home

import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item

data class MainViewModel(
    val data: Map<Data, List<Item>>?,
    val isLoading: Boolean,
    val apiError: Throwable?
)

sealed class MainViewAction {
    object Load : MainViewAction()
    object ReLoad : MainViewAction()
}

sealed class MainViewResult {
    object LoadingStarted: MainViewResult()
    data class ErrorLoading(val error: Throwable) : MainViewResult()
    data class SuccessLoading(val data: Map<Data, List<Item>>) : MainViewResult()
}