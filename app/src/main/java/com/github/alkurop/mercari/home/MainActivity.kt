package com.github.alkurop.mercari.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.github.alkurop.mercari.App
import com.github.alkurop.mercari.R
import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.utils.disposedBy
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val actionsPublisher = PublishSubject.create<MainViewAction>()
    private val compositeDisposable = CompositeDisposable()
    @Inject
    lateinit var interactor: MainViewInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.appComponent.inject(this)
        swipeLayout.setOnRefreshListener {
            actionsPublisher.onNext(MainViewAction.ReLoad)
        }
        swipeLayout.isEnabled = false
    }

    override fun onStart() {
        super.onStart()
        actionsPublisher
            .startWith(MainViewAction.Load)
            .compose(interactor.process())
            .compose(mapMainResult())
            .distinctUntilChanged()
            .observeOn(mainThread())
            .subscribe { renderView(it) }
            .disposedBy(compositeDisposable)
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    fun renderView(mainViewModel: MainViewModel) {
        mainViewModel.data?.let {
            renderData(it)
        }
        mainViewModel
            .takeIf { it.isLoading }
            ?.let { swipeLayout.isEnabled = false }

        mainViewModel
            .takeIf { it.isLoading }
            ?.takeIf { swipeLayout.isRefreshing.not() }
            ?.let {
                progress.visibility = View.VISIBLE
            }

        mainViewModel
            .takeIf { it.isLoading.not() }
            ?.let {
                swipeLayout.isEnabled = true
                progress.visibility = View.GONE
                swipeLayout.isRefreshing = false
            }

        mainViewModel.apiError?.let {
            it.printStackTrace()
        }

        val isEmpty = mainViewModel.isLoading.not() && mainViewModel.apiError != null
        emptyView.visibility = if(isEmpty) View.VISIBLE else View.GONE

    }

    fun renderData(data: Map<Data, List<Item>>) {
        val items = data.keys.toList()
        val pagerAdapter = TabsAdapter(supportFragmentManager, items.map { it.name })
        pager.adapter = pagerAdapter
        tabs.setupWithViewPager(pager, true)
    }
}
