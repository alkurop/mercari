package com.github.alkurop.mercari.home

import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction

fun mapMainResult(): ObservableTransformer<MainViewResult, MainViewModel> {
    return ObservableTransformer { upstream ->
        upstream
            .scan(getDefaultViewModel(), getMapper())
    }
}

private fun getMapper(): BiFunction<MainViewModel, in MainViewResult, MainViewModel> {
    return BiFunction { oldModel, result ->
        when (result) {
            is MainViewResult.LoadingStarted ->
                oldModel.copy(isLoading = true, apiError = null)
            is MainViewResult.ErrorLoading ->
                oldModel.copy(isLoading = false, apiError = result.error)
            is MainViewResult.SuccessLoading ->
                oldModel.copy(result.data, isLoading = false, apiError = null)
        }
    }
}

private fun getDefaultViewModel(): MainViewModel {
    return MainViewModel(null, true, null)
}
