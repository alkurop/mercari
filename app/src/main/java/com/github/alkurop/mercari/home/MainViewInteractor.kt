package com.github.alkurop.mercari.home

import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.repo.DataRepo
import com.github.alkurop.mercari.repo.ErrorRepo
import com.github.alkurop.mercari.repo.ItemsRepo
import com.github.alkurop.mercari.usecase.LoadDataUC
import com.github.alkurop.mercari.usecase.LoadItemsUC
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import javax.inject.Inject

class MainViewInteractor @Inject constructor(
    private val scheduler: Scheduler,
    private val dataRepo: DataRepo,
    private val errorRepo: ErrorRepo,
    private val itemsRepo: ItemsRepo,
    private val loadDataUC: LoadDataUC,
    private val loadItemsUC: LoadItemsUC
) {
    fun process(): ObservableTransformer<MainViewAction, MainViewResult> {
        return ObservableTransformer { upstream ->
            upstream.observeOn(scheduler)
                .publish { shared ->
                    Observable.merge<MainViewResult>(
                        actionMapper(shared),
                        repoMapper()
                    )
                }
        }
    }

    private fun actionMapper(shared: Observable<MainViewAction>): Observable<MainViewResult> {
        return Observable.merge(
            shared.ofType(MainViewAction.ReLoad::class.java)
                .flatMap {
                    Observable.merge(
                        Observable.just(MainViewResult.LoadingStarted),
                        loadDataUC.execute().toObservable()
                    )
                },

            shared.ofType(MainViewAction.Load::class.java)
                .flatMap {
                    if (dataRepo.hasValue()) Observable.empty()
                    else Observable.merge(
                        Observable.just(MainViewResult.LoadingStarted),
                        loadDataUC
                            .execute()
                            .toObservable<MainViewResult.LoadingStarted>()
                    )
                }
        )
    }

    private fun repoMapper(): Observable<MainViewResult> {
        return Observable.merge(
            dataRepo.observe()
                .switchMap { domains ->
                    Observable
                        .zip(domains.map { domain ->
                            itemsRepo.observe(domain.name)
                                .map { result ->
                                    Pair(domain, result)
                                }
                        }) {
                            @Suppress("UNCHECKED_CAST")
                            (it as Array<out Any>)
                                .toList()
                                .map { it as Pair<Data, List<Item>> }
                        }
                }
                .map {
                    it
                        .fold(mutableMapOf<Data, List<Item>>()) { map, data ->
                            map[data.first] = data.second; map
                        }
                }
                .map { MainViewResult.SuccessLoading(it) },

            dataRepo.observe()
                .flatMapCompletable { loadItemsUC.execute(it) }
                .toObservable(),

            errorRepo.observe()
                .filter { oneShot -> oneShot.isShot().not() }
                .map { MainViewResult.ErrorLoading(it.getValueOnce()) }
        )
    }
}