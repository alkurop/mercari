package com.github.alkurop.mercari.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.github.alkurop.mercari.list.ItemListFragment

class TabsAdapter(fm: FragmentManager, val items: List<String>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val listName = items[position]
        return ItemListFragment.newInstance(listName)
    }

    override fun getCount(): Int = items.size

    override fun getItemPosition(`object`: Any): Int = PagerAdapter.POSITION_NONE

    override fun getPageTitle(position: Int): CharSequence? {
        return items[position].capitalize()
    }
}
