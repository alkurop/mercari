package com.github.alkurop.mercari.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.alkurop.mercari.R
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.domain.Status
import kotlinx.android.synthetic.main.view_item.view.*

class ItemAdapter(val items: List<Item>) : RecyclerView.Adapter<ItemsVH>() {
    private lateinit var li: LayoutInflater

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        li = LayoutInflater.from(recyclerView.context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ItemsVH {
        val view = li.inflate(R.layout.view_item, parent, false)
        return ItemsVH(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(viewHolder: ItemsVH, index: Int) {
        viewHolder.bind(items[index])
    }
}

class ItemsVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: Item) {
        itemView.apply {
            titleView.text = item.name
            soldOutView.visibility =
                    if (item.status == Status.SOLD_OUT) View.VISIBLE
                    else View.GONE

            Glide.with(itemView).load(item.photoUrl).into(posterView)
            likesCount.text = item.likeCount.toString()
            commentsCount.text = item.commentCount.toString()
            price.text = "$ ${item.price}"
        }
    }
}