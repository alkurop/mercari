package com.github.alkurop.mercari.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.github.alkurop.mercari.App
import com.github.alkurop.mercari.R
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.utils.disposedBy
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject


class ItemListFragment : Fragment() {

    companion object {
        private const val KEY_NAME = "key_name"

        fun newInstance(listName: String): ItemListFragment {
            val args = Bundle(1)
            args.putString(KEY_NAME, listName)
            val fragment = ItemListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private val compositeDisposable = CompositeDisposable()
    private val actions = PublishSubject.create<ListFragmentAction>()
    private val updates = BehaviorSubject.create<List<Item>>()

    @Inject
    lateinit var interactor: ListFragmentInteractor
    private lateinit var name: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        name = arguments!!.getString(KEY_NAME)!!
        recyclerView.layoutManager = GridLayoutManager(context, if (isLandscapeMode()) 4 else 2)
        App.appComponent.inject(this)
    }

    override fun onStart() {
        super.onStart()
        actions
            .startWith(ListFragmentAction.Load(name))
            .compose(interactor.process())
            .compose(mapListFragmentResult())
            .observeOn(mainThread())
            .subscribe { viewModel ->
                renderView(viewModel)
            }
            .disposedBy(compositeDisposable)

        updates
            .distinctUntilChanged()
            .subscribe {
                renderItems(it)
            }
            .disposedBy(compositeDisposable)
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    private fun renderItems(item: List<Item>) {
        val adapter = ItemAdapter(item)
        recyclerView.adapter = adapter
    }

    private fun renderView(viewModel: ListFragmenViewModel) {
        viewModel.items?.let {
            updates.onNext(it)
            emptyView.visibility = View.GONE
        }
        viewModel.items
            .orEmpty()
            .takeIf { it.isEmpty() }
            ?.let {
                emptyView.visibility = View.VISIBLE
            }
    }

    private fun isLandscapeMode(): Boolean {
        val width = activity!!.windowManager.defaultDisplay.width
        val height = activity!!.windowManager.defaultDisplay.height
        return width > height
    }
}