package com.github.alkurop.mercari.list

import com.github.alkurop.mercari.domain.Item

sealed class ListFragmentAction {
    data class Load(val name: String) : ListFragmentAction()
}

sealed class ListFragmentResult {
    data class DataLoaded(val items: List<Item>) : ListFragmentResult()
    data class DataLoadError(val throwable: Throwable) : ListFragmentResult()
}

data class ListFragmenViewModel(
    val items: List<Item>?,
    val error: Throwable?
)