package com.github.alkurop.mercari.list

import com.github.alkurop.mercari.repo.ErrorRepo
import com.github.alkurop.mercari.repo.ItemsRepo
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import javax.inject.Inject

class ListFragmentInteractor @Inject constructor(
    private val itemsRepo: ItemsRepo,
    private val errorRepo: ErrorRepo,
    private val scheduler: Scheduler
) {
    fun process(): ObservableTransformer<ListFragmentAction, ListFragmentResult> {
        return ObservableTransformer { shared ->
            shared.observeOn(scheduler)
                .publish { upstream ->
                    upstream
                        .ofType(ListFragmentAction.Load::class.java)
                        .switchMap { loadAction ->
                            itemsRepo.observe(loadAction.name)
                                .map { data -> ListFragmentResult.DataLoaded(data) }
                                .map { it as ListFragmentResult }
                                .mergeWith(errorRepo.observe()
                                    .filter { error -> error.isShot().not() }
                                    .map { error -> ListFragmentResult.DataLoadError(error.checkValue()) })
                        }


                }
        }
    }
}