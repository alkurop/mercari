package com.github.alkurop.mercari.list

import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction

fun mapListFragmentResult(): ObservableTransformer<ListFragmentResult, ListFragmenViewModel> {
    return ObservableTransformer { upstream ->
        upstream.scan(getDefaultViewModel(), getMapper())
    }

}

fun getMapper(): BiFunction<ListFragmenViewModel, in ListFragmentResult, ListFragmenViewModel>? {

    return BiFunction { oldModel, result ->
        when (result) {
            is ListFragmentResult.DataLoaded -> {
                oldModel.copy(result.items, null)
            }
            is ListFragmentResult.DataLoadError -> {
                oldModel.copy(error = result.throwable)
            }
        }
    }
}


private fun getDefaultViewModel(): ListFragmenViewModel {
    return ListFragmenViewModel(null, null)
}