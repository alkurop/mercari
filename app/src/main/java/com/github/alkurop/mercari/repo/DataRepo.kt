package com.github.alkurop.mercari.repo

import com.github.alkurop.mercari.domain.Data
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepo @Inject constructor() {

    private val subject = BehaviorSubject.create<List<Data>>()

    fun hasValue(): Boolean {
        return subject.hasValue()
    }

    fun observe(): Observable<List<Data>> {
        return subject
    }

    fun newValue(data: List<Data>) {
        subject.onNext(data)
    }
}