package com.github.alkurop.mercari.repo

import com.github.alkurop.mercari.OneShot
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorRepo @Inject constructor() {

    private val subject = BehaviorSubject.create<OneShot<Throwable>>()

    fun hasValue(): Boolean {
        return subject.hasValue()
    }

    fun observe(): Observable<OneShot<Throwable>> {
        return subject
    }

    fun newValue(value: Throwable) {
        subject.onNext(OneShot(value))
    }
}