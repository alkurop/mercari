package com.github.alkurop.mercari.repo

import com.github.alkurop.mercari.domain.Item
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ItemsRepo @Inject constructor() {

    private val subjectMap = mutableMapOf<String, BehaviorSubject<List<Item>>>()

    fun hasValue(domain: String): Boolean {
        return getSubject(domain).hasValue()
    }

    fun observe(domain: String): Observable<List<Item>> {
        return getSubject(domain)
    }

    fun newValue(domain: String, data: List<Item>) {
        getSubject(domain).onNext(data)
    }

    @Synchronized
    private fun getSubject(domain: String): BehaviorSubject<List<Item>> {
        return if (subjectMap.containsKey(domain)) {
            subjectMap[domain]!!
        } else {
            val sub = BehaviorSubject.create<List<Item>>()
            subjectMap[domain] = sub
            sub
        }
    }
}