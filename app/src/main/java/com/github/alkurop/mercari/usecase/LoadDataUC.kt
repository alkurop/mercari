package com.github.alkurop.mercari.usecase

import com.github.alkurop.mercari.api.Api
import com.github.alkurop.mercari.repo.DataRepo
import com.github.alkurop.mercari.repo.ErrorRepo
import io.reactivex.Completable
import javax.inject.Inject

class LoadDataUC @Inject constructor(
    private val api: Api,
    private val dataRepo: DataRepo,
    private val errorRepo: ErrorRepo
) {
    fun execute(): Completable {
        return api.getData()
            .flatMapCompletable { result ->
                Completable.fromAction {
                    if (dataRepo.hasValue()) {
                        val cachedValue = dataRepo.observe().blockingFirst()
                        if (cachedValue != result.asList()) {
                            dataRepo.newValue(result.asList())
                        }
                    } else {
                        dataRepo.newValue(result.asList())
                    }
                }
            }
            .onErrorResumeNext { throwable ->
                Completable.fromAction {
                    errorRepo.newValue(throwable)
                }
            }
    }
}