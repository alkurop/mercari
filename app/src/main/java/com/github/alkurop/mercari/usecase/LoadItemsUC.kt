package com.github.alkurop.mercari.usecase

import com.github.alkurop.mercari.api.Api
import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.repo.ErrorRepo
import com.github.alkurop.mercari.repo.ItemsRepo
import io.reactivex.Completable
import io.reactivex.Scheduler
import javax.inject.Inject

class LoadItemsUC @Inject constructor(
    private val api: Api,
    private val itemsRepo: ItemsRepo,
    private val errorRepo: ErrorRepo,
    private val scheduler: Scheduler

) {
    fun execute(domainList: List<Data>): Completable {
        val completables = domainList.map { domain ->
            api.getItems(domain.dataUrl)
                .subscribeOn(scheduler)
                .flatMapCompletable { result ->
                    Completable.fromAction {
                        val resultAsList = result.asList()
                        if (itemsRepo.hasValue(domain.name)) {
                            val cachedValue = itemsRepo
                                .observe(domain.name)
                                .blockingFirst()
                            if (cachedValue != resultAsList) {
                                itemsRepo.newValue(domain.name, resultAsList)
                            }
                        } else {
                            itemsRepo.newValue(domain.name, resultAsList)
                        }
                    }
                }
        }
        return Completable
            .merge(completables)
            .onErrorResumeNext { throwable ->
                Completable
                    .fromAction {
                        errorRepo.newValue(throwable)
                    }
            }
    }
}