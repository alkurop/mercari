package com.github.alkurop.mercari

import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.domain.Status

val mockData = Data("test", "test")
val mockItem = Item(
    "test",
    "test",
    Status.SOLD_OUT,
    0,
    0,
    0,
    "test"
)
val mockError = RuntimeException("test")

