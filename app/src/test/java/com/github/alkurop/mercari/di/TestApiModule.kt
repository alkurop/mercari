package com.github.alkurop.mercari.di

import com.github.alkurop.mercari.api.Api
import com.github.alkurop.mercari.mockData
import com.github.alkurop.mercari.mockItem
import dagger.Module
import dagger.Provides
import io.reactivex.Single
import org.mockito.Mockito
import org.mockito.Mockito.mock
import javax.inject.Singleton

@Module
class TestApiModule {
    @Provides
    @Singleton
    fun provideApi(): Api {
        val api = mock(Api::class.java)

        Mockito.`when`(api.getItems(mockData.name))
            .thenReturn(Single.just(arrayOf(mockItem)))

        Mockito.`when`(api.getData())
            .thenReturn(Single.just(arrayOf(mockData)))

        return api
    }
}