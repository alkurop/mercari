package com.github.alkurop.mercari.di

import com.github.alkurop.mercari.home.MainResultMapperTest
import com.github.alkurop.mercari.home.MainViewInteractorTest
import com.github.alkurop.mercari.list.ListFragmentInteractorTest
import com.github.alkurop.mercari.usecase.LoadDataUCTest
import com.github.alkurop.mercari.usecase.LoadItemsUCTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TestApiModule::class, TestDependencyModule::class])
interface TestAppComponent {

    fun inject(mainResultMapperTest: MainResultMapperTest)

    fun inject(mainResultMapperTest: LoadDataUCTest)

    fun inject(loadItemsUCTest: LoadItemsUCTest)

    fun inject(listFragmentInteractorTest: ListFragmentInteractorTest)

    fun inject(mainViewInteractorTest: MainViewInteractorTest)

}