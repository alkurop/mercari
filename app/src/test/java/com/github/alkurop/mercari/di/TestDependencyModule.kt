package com.github.alkurop.mercari.di

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class TestDependencyModule{
    @Singleton
    @Provides
    fun provideScheduler(): Scheduler {
        return Schedulers.trampoline()
    }
}