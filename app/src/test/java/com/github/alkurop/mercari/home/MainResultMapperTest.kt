package com.github.alkurop.mercari.home

import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import java.lang.RuntimeException

class MainResultMapperTest {

    private val resultPublisher = PublishSubject.create<MainViewResult>()
    private lateinit var test: TestObserver<MainViewModel>

    @Before
    fun setUp() {
        test = resultPublisher
            .compose(mapMainResult())
            .test()
    }

    @Test
    fun `test Mapper starts with default view model`() {
        test
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(1)
    }

    @Test
    fun `test LoadingStarted result mapped`() {
        val loadingStarted = MainViewResult.LoadingStarted

        //when
        resultPublisher.onNext(loadingStarted)

        //then
        test.assertNoErrors()
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(2)
            .assertValueAt(1) {
                it.isLoading && it.apiError == null && it.data == null
            }
    }

    @Test
    fun `test ErrorLoading result mapped`() {
        val error = RuntimeException("test")
        val loadingError = MainViewResult.ErrorLoading(error)

        //when
        resultPublisher.onNext(loadingError)

        //then
        test.assertNoErrors()
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(2)
            .assertValueAt(1) {
                it.isLoading.not() && it.apiError == error && it.data == null
            }
    }

    @Test
    fun `test SuccessLoading result mapped`() {
        val success = mapOf<Data, List<Item>>()
        val loadingError = MainViewResult.SuccessLoading(success)

        //when
        resultPublisher.onNext(loadingError)

        //then
        test.assertNoErrors()
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(2)
            .assertValueAt(1) {
                it.isLoading.not() && it.apiError == null && it.data == success
            }
    }
}