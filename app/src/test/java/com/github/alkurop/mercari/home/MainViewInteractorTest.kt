package com.github.alkurop.mercari.home

import com.github.alkurop.mercari.api.Api
import com.github.alkurop.mercari.di.DaggerTestAppComponent
import com.github.alkurop.mercari.mockData
import com.github.alkurop.mercari.mockError
import com.github.alkurop.mercari.mockItem
import com.github.alkurop.mercari.repo.DataRepo
import com.github.alkurop.mercari.repo.ErrorRepo
import com.github.alkurop.mercari.repo.ItemsRepo
import dagger.multibindings.IntKey
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import javax.inject.Inject

class MainViewInteractorTest {
    @Inject
    lateinit var mainViewInteractor: MainViewInteractor

    @Inject
    lateinit var dataRepo: DataRepo

    @Inject
    lateinit var errorRepo: ErrorRepo

    @Inject
    lateinit var api: Api

    private lateinit var test: TestObserver<MainViewResult>

    private val actionsSubject = PublishSubject.create<MainViewAction>()

    @Before
    fun setUp() {
        DaggerTestAppComponent.create().inject(this)

        test = actionsSubject
            .compose(mainViewInteractor.process())
            .test()
    }

    @Test
    fun `interactor receives DataRepo events and Starts LoadItemsUC`() {
        dataRepo.newValue(listOf(mockData))

        //then
        verify(api).getItems(mockData.dataUrl)

    }

    @Test
    fun `interactor receives ErrorRepo events`() {
        errorRepo.newValue(mockError)

        //then
        test.assertValueCount(1)
            .assertValueAt(0) { value ->
                value is MainViewResult.ErrorLoading
                        && value.error == mockError
            }
    }

    @Test
    fun `interactor executes LoadData on Load action and recievs ItemsRepo result`() {
        actionsSubject.onNext(MainViewAction.Load)

        //then
        test.assertValueCount(2)
            .assertValueAt(0) { it is MainViewResult.LoadingStarted }
            .assertValueAt(1) {
                it is MainViewResult.SuccessLoading
                        && it.data == mapOf(Pair(mockData, listOf(mockItem)))
            }
    }

    @Test
    fun `interactor does not execute LoadItemsUC on Load action when DataRepo has value`() {
        actionsSubject.onNext(MainViewAction.Load)
        actionsSubject.onNext(MainViewAction.Load)

        //then
        test.assertValueCount(2)

    }

    @Test
    fun `interactor executes LoadItemsUC on ReLoad action `() {
        actionsSubject.onNext(MainViewAction.ReLoad)

        //then
        test.assertValueCount(2)
            .assertValueAt(0) { it is MainViewResult.LoadingStarted }
            .assertValueAt(1) {
                it is MainViewResult.SuccessLoading
                        && it.data == mapOf(Pair(mockData, listOf(mockItem)))
            }
    }
}