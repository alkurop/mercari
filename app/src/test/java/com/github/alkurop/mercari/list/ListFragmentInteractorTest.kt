package com.github.alkurop.mercari.list

import com.github.alkurop.mercari.di.DaggerTestAppComponent
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.mockError
import com.github.alkurop.mercari.repo.ErrorRepo
import com.github.alkurop.mercari.repo.ItemsRepo
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class ListFragmentInteractorTest {

    @Inject
    lateinit var listFragmentInteractor: ListFragmentInteractor

    @Inject
    lateinit var itemsRepo: ItemsRepo

    @Inject
    lateinit var errorRepo: ErrorRepo

    private val subject = PublishSubject.create<ListFragmentAction>()
    private lateinit var test: TestObserver<ListFragmentResult>

    @Before
    fun setUp() {
        DaggerTestAppComponent.create().inject(this)
        test = subject
            .compose(listFragmentInteractor.process())
            .test()
    }

    @Test
    fun `interactor receives ItemsRepo events`() {
        subject.onNext(ListFragmentAction.Load("test"))
        val data = listOf<Item>()

        //when
        itemsRepo.newValue("test", data)

        //then
        test
            .assertNoErrors()
            .assertNotComplete()
            .assertValueCount(1)
            .assertValue {
                it is ListFragmentResult.DataLoaded
                        && it.items == data
            }
    }

    @Test
    fun `interactor receives ErrorRepo events`() {
        subject.onNext(ListFragmentAction.Load("test"))

        //when
        errorRepo.newValue(mockError)

        //then
        test
            .assertNoErrors()
            .assertNotComplete()
            .assertValueCount(1)
            .assertValue {
                it is ListFragmentResult.DataLoadError
                        && it.throwable == mockError
            }
    }
}