package com.github.alkurop.mercari.list

import com.github.alkurop.mercari.domain.Item
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test

class ListFragmentResultMapperTest {
    private val resultPublisher = PublishSubject.create<ListFragmentResult>()
    private lateinit var test: TestObserver<ListFragmenViewModel>

    @Before
    fun setUp() {
        test = resultPublisher
            .compose(mapListFragmentResult())
            .test()
    }

    @Test
    fun `test Mapper starts with default view model`() {
        test
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(1)
    }

    @Test
    fun `test DataLoadError result mapped`() {
        val error = RuntimeException("test")
        val loadingError = ListFragmentResult.DataLoadError(error)

        //when
        resultPublisher.onNext(loadingError)

        //then
        test.assertNoErrors()
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(2)
            .assertValueAt(1) {
                it.error == error && it.items == null
            }
    }

    @Test
    fun `test SuccessLoading result mapped`() {
        val success = listOf<Item>()
        val loadingError = ListFragmentResult.DataLoaded(success)

        //when
        resultPublisher.onNext(loadingError)

        //then
        test.assertNoErrors()
            .assertNotComplete()
            .assertNoErrors()
            .assertValueCount(2)
            .assertValueAt(1) {
                it.error == null && it.items == success
            }
    }
}