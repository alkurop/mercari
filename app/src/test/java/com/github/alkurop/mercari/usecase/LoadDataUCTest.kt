package com.github.alkurop.mercari.usecase

import com.github.alkurop.mercari.api.Api
import com.github.alkurop.mercari.di.DaggerTestAppComponent
import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.repo.DataRepo
import com.github.alkurop.mercari.repo.ErrorRepo
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.lang.RuntimeException
import javax.inject.Inject

class LoadDataUCTest {

    @Inject
    lateinit var errorRepo: ErrorRepo
    @Inject
    lateinit var dataRepo: DataRepo

    @Mock
    private lateinit var api: Api
    private lateinit var loadDataUC: LoadDataUC

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        DaggerTestAppComponent.create().inject(this)
        loadDataUC = LoadDataUC(api, dataRepo, errorRepo)
    }

    @Test
    fun `on api error post result to ErrorRepo`() {
        val error = RuntimeException("test")
        `when`(api.getData()).thenReturn(Single.error(error))

        //when
        loadDataUC.execute().subscribe()

        //then
        errorRepo.observe()
            .test()
            .assertValueCount(1)
            .assertValue { oneShot ->
                oneShot.getValueOnce() == error
            }
    }

    @Test
    fun `on api success post result to DataRepo`() {
        val success = arrayOf<Data>()
        `when`(api.getData()).thenReturn(Single.just(success))

        //when
        loadDataUC.execute().subscribe()

        //then
        dataRepo.observe()
            .test()
            .assertValueCount(1)
            .assertValue { data ->
                data == success.toList()
            }
    }

    @Test
    fun `usecase does not post same data twice to DataRepo`() {
        val success = arrayOf<Data>()
        `when`(api.getData()).thenReturn(Single.just(success))

        //when
        loadDataUC.execute().subscribe()
        loadDataUC.execute().subscribe()

        //then
        dataRepo.observe()
            .test()
            .assertValueCount(1)
    }
}