package com.github.alkurop.mercari.usecase

import com.github.alkurop.mercari.api.Api
import com.github.alkurop.mercari.di.DaggerTestAppComponent
import com.github.alkurop.mercari.domain.Data
import com.github.alkurop.mercari.domain.Item
import com.github.alkurop.mercari.repo.ErrorRepo
import com.github.alkurop.mercari.repo.ItemsRepo
import io.reactivex.Scheduler
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import javax.inject.Inject

class LoadItemsUCTest {

    @Inject
    lateinit var errorRepo: ErrorRepo
    @Inject
    lateinit var itemsRepo: ItemsRepo
    @Inject
    lateinit var scheduler: Scheduler

    @Mock
    private lateinit var api: Api
    private lateinit var loadItemsUC: LoadItemsUC

    private val mockData = Data("test", "test")

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        DaggerTestAppComponent.create().inject(this)
        loadItemsUC = LoadItemsUC(api, itemsRepo, errorRepo, scheduler)
    }

    @Test
    fun `on api error post result to ErrorRepo`() {
        val error = RuntimeException("test")
        `when`(api.getItems(mockData.dataUrl)).thenReturn(Single.error(error))

        //when
        loadItemsUC.execute(listOf(mockData)).subscribe()

        //then
        errorRepo.observe()
            .test()
            .assertValueCount(1)
            .assertValue { oneShot ->
                oneShot.getValueOnce() == error
            }
    }

    @Test
    fun `on api success post result to ItemsRepo`() {
        val success = arrayOf<Item>()
        `when`(api.getItems(mockData.dataUrl)).thenReturn(Single.just(success))

        //when
        loadItemsUC.execute(listOf(mockData)).subscribe()

        //then
        itemsRepo.observe(mockData.name)
            .test()
            .assertValueCount(1)
            .assertValue { data ->
                data == success.toList()
            }
    }

    @Test
    fun `usecase does not post same data twice to ItemsRepo`() {
        val success = arrayOf<Item>()
        `when`(api.getItems(mockData.dataUrl)).thenReturn(Single.just(success))

        //when
        loadItemsUC.execute(listOf(mockData)).subscribe()
        loadItemsUC.execute(listOf(mockData)).subscribe()

        itemsRepo.observe(mockData.name)
            .test()
            .assertValueCount(1)
    }
}
